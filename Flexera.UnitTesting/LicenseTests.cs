﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Flexera.Library.Helpers;
using System.IO;

namespace Flexera.UnitTesting
{
    [TestClass]
    public class LicenseTests
    {
        [TestMethod]
        public void Test_Application_Count_With_Single_User()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-1.csv";
            int applicationId = 374;
            int expectedResult = 1;

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);


            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void Test_Application_Count_With_Multiple_Users()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-2.csv";
            int applicationId = 374;
            int expectedResult = 3;

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);

            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void Test_Application_Count_With_Duplicates_Records()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-3.csv";
            int applicationId = 374;
            int expectedResult = 2;

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);

            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void Test_Application_Count_With_Multiple_Users_And_Duplicate_Records()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-4.csv";
            int applicationId = 374;
            int expectedResult = 5;

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);

            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void Test_Application_Count_With_Invalid_AppID()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-6.csv";
            int applicationId = -9999999;
            int expectedResult = 0;

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);

            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]       
        public void Test_Application_Count_With_Empty_File()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-5.csv";
            int applicationId = 374;
            int expectedResult = 0;

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);

            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void Test_Application_Count_With_Unavailable_File()
        {
            //arrange test data
            string fileName = Directory.GetCurrentDirectory() + "\\TestCases\\test-case-invalid.csv";
            int applicationId = 374;           

            LicenseHelper helper = new LicenseHelper();
            int actualResult = helper.GetLicenseCountForCompany(fileName, applicationId);           

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flexera.Library.Interfaces
{
    interface ILicenseHelper
    {
        int GetLicenseCountForCompany(string strFileName, int iApplicationID);
    }
}

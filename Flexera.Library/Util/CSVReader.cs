﻿using Flexera.Library.Models;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flexera.Library.Util
{
    class CSVReader
    {
        private const int COMPUTER_ID_POS = 0;
        private const int USER_ID_POS = 1;
        private const int APPLICATION_ID_POS = 2;
        private const int COMPUTER_TYPE_POS = 3;
        private const int COMMENT_POS = 4;

        /// <summary>
        /// Method read installaion data from given csv file
        /// </summary>
        /// <param name="strFileName">Absolute file name</param>
        /// <param name="iApplicationID">Targeted Application Id to read data</param>
        /// <returns></returns>
        public static List<InstallationInfo> ReadInstallationInfo(string strFileName, int iApplicationID)
        {
            List<InstallationInfo> lstInfo = new List<InstallationInfo>();
            TextFieldParser parser = null;
            try
            {
                parser = new TextFieldParser(strFileName);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                int x = 0;
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();

                    //avoid first row.
                    if (x > 0)
                    {
                        //considering only the given AppId
                        if (Convert.ToInt32(fields[APPLICATION_ID_POS]) == iApplicationID)
                        {
                            lstInfo.Add(new InstallationInfo
                            {
                                ComputerId = Convert.ToInt32(fields[COMPUTER_ID_POS]),
                                UserId = Convert.ToInt32(fields[USER_ID_POS]),
                                ApplicationId = Convert.ToInt32(fields[APPLICATION_ID_POS]),
                                ComputerType = fields[COMPUTER_TYPE_POS].ToUpper(),
                                Comment = fields[COMMENT_POS]
                            });
                        }
                    }
                    x++;

                }
            }
            catch(FileNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                //Any other errors are writing to event-log
                EventLog.WriteEntry("FLEXERA", ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                if (parser != null)
                {
                    parser.Close();
                }
            }
            return lstInfo;
        }
    }
}

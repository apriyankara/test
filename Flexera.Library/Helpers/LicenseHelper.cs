﻿using Flexera.Library.Interfaces;
using Flexera.Library.Models;
using Flexera.Library.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flexera.Library.Helpers
{
    public class LicenseHelper:ILicenseHelper
    {
        /// <summary>
        /// Method count minimum number of license required for given application
        /// </summary>
        /// <param name="strFileName">Absolute file name</param>
        /// <param name="iApplicationID">Targeted Application Id</param>
        /// <returns>License Count</returns>
        public int GetLicenseCountForCompany(string strFileName, int iApplicationID)
        {
            int iLicenseCount = 0;

            //extract only the records with given App Id, otherwise memory usage will be high when reading a large file
            List<InstallationInfo> lstInfo = CSVReader.ReadInstallationInfo(strFileName, iApplicationID);

            //remove duplicates
            lstInfo = lstInfo.GroupBy(x => new { x.ComputerId, x.UserId, x.ApplicationId, x.ComputerType })
                           .Select(group => group.First()).ToList();

            if (lstInfo != null && lstInfo.Count > 0)
            {
                //group records by userID
                var userGroups = from v in lstInfo group v by v.UserId;

                foreach (var userGroup in userGroups)
                {
                    //get application count required for each user
                    iLicenseCount += GetLicenseCountForUser(userGroup.Key, userGroup.ToList<InstallationInfo>());
                }
            }

            return iLicenseCount;
        }
     
        private int GetLicenseCountForUser(int userId, List<InstallationInfo> lstUserEntries)
        {
            int iNumOfLicenses = 0;

            int iLaptopCount = (from v in lstUserEntries where v.ComputerType.ToUpper() == "LAPTOP" select v).Count();
            int iDesktopCount = (from v in lstUserEntries where v.ComputerType.ToUpper() != "LAPTOP" select v).Count();

            if (iLaptopCount == iDesktopCount)
            {
                iNumOfLicenses = iLaptopCount;
            }
            else if (iLaptopCount > iDesktopCount)
            {
                iNumOfLicenses = iDesktopCount + ((iLaptopCount - iDesktopCount) % 2 == 0 ? (iLaptopCount - iDesktopCount) / 2 : ((iLaptopCount + 1) - iDesktopCount) / 2);
            }
            else if (iLaptopCount < iDesktopCount)
            {
                iNumOfLicenses = iLaptopCount + (iDesktopCount - iLaptopCount);
            }

            return iNumOfLicenses;
        }
    }
}

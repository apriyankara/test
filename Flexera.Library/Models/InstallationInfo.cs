﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flexera.Library.Models
{
    class InstallationInfo
    {
        public int ComputerId { get; set; }
        public int UserId { get; set; }
        public int ApplicationId { get; set; }
        public string ComputerType { get; set; }
        public string Comment { get; set; }
    }
}
